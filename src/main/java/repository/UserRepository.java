package repository;

import model.User;

import java.sql.SQLException;
import java.util.List;

public interface UserRepository {
    User findOne(Long id) throws SQLException;

    int create(User user) throws SQLException;

    List<User> readAll() throws SQLException;

    int update(User user) throws SQLException;

    int delete(User user) throws SQLException;
}
