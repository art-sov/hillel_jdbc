package repository.impl;

import model.User;
import repository.UserRepository;
import repository.constanst.UserConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static repository.config.Connection.getConnection;

public class UserRepositoryImpl implements UserRepository{

    private Logger log = Logger.getLogger(UserRepositoryImpl.class.getName());

    @Override
    public User findOne(Long id) throws SQLException {

        User user = new User();

        try (java.sql.Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UserConstants.STMT_FIND_ONE)){

            //SELECT id, name, lastname, password, email, last_active, project_id FROM users WHERE id=?;

            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                log.log(Level.FINE, "user with id = " + id + " does not exists");
                return null;
            }

            user.setId(resultSet.getLong("id"));

            user.setName(resultSet.getString("name"));
            user.setLastname(resultSet.getString("lastname"));
            user.setPassword(resultSet.getString("password"));
            user.setEmail(resultSet.getString("email"));
            user.setLastActive(resultSet.getString("last_active"));
            user.setProject_id(resultSet.getLong("project_id"));

            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "method findOne, possible problems with the database");
        }
        return user;
    }

    @Override
    public int create(User user) throws SQLException {
        int resultCreate = 0;
        try (java.sql.Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UserConstants.STMT_INSERT_ONE)){

            //INSERT INTO users (name, lastname, password, email, last_active, project_id) VALUES (?, ?, ?, ?, ?, ?);

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLastname());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getLastActive());
            preparedStatement.setLong(6, user.getProject_id());

            resultCreate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method create, possible problems with the database");
        }
        return resultCreate;
    }

    @Override
    public List<User> readAll() throws SQLException {
        List<User> userList = new ArrayList<>();

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(UserConstants.STMT_READ_ALL);

            while (resultSet.next()){
                User user = new User();

                user.setId(resultSet.getLong("id"));
                user.setName(resultSet.getString("name"));
                user.setLastname(resultSet.getString("lastname"));
                user.setPassword(resultSet.getString("password"));
                user.setEmail(resultSet.getString("email"));
                user.setLastActive(resultSet.getString("last_active"));
                user.setProject_id(resultSet.getLong("project_id"));

                userList.add(user);
                resultSet.close();
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method readAll, possible problems with the database");

        }
        return userList;    }

    @Override
    public int update(User user) throws SQLException {
        int resultUpdate = 0;
        try (java.sql.Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(UserConstants.STMT_UPDATE)){

            //UPDATE users SET name=?, lastname=?, password=?, email=?, last_active=?, project_id=? WHERE id=?;

            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getLastname());
            preparedStatement.setString(3, user.getPassword());
            preparedStatement.setString(4, user.getEmail());
            preparedStatement.setString(5, user.getLastActive());
            preparedStatement.setLong(6,  user.getProject_id());
            preparedStatement.setLong(7, user.getId());

            resultUpdate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method update, possible problems with the database");
        }
        return resultUpdate;
    }

    @Override
    public int delete(User user) throws SQLException {
        int resultDelete = 0;

        try (java.sql.Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UserConstants.STMT_DELETE)) {
            //DELETE FROM users WHERE id=?

            preparedStatement.setLong(1, user.getId());
            resultDelete = preparedStatement.executeUpdate();

            if (resultDelete == 0)
                log.log(Level.FINE, "User with that id does not exists");


        } catch (SQLFeatureNotSupportedException e){
            log.log(Level.SEVERE,"method delete, passible? not found id");

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method delete, possible problems with the database");
        }
        return resultDelete;
    }


    public Long getIdLastUser () throws SQLException{

        Long id = null;

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(UserConstants.STMT_LAST_ID);

            if (resultSet.next())
                id = resultSet.getLong(1);
            else
                return null;
            resultSet.close();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method getIdLastUser, possible problems with the database");
        }
        return id;
    }
}
