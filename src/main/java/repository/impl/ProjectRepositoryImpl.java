package repository.impl;

import model.Project;
import repository.ProjectRepository;
import repository.constanst.ProjectConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static repository.config.Connection.getConnection;

public class ProjectRepositoryImpl implements ProjectRepository{
    private Logger log = Logger.getLogger(ProjectRepositoryImpl.class.getName());

    @Override
    public Project findOne(Long id) {
        Project project = new Project();

        try (java.sql.Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(ProjectConstants.STMT_FIND_ONE)){

            //SELECT id, name, type, url FROM projects WHERE id=?;

            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                log.log(Level.FINE, "project with id = " + id + " does not exists");
                return null;
            }

            project.setId(resultSet.getLong("id"));

            project.setName(resultSet.getString("name"));
            project.setType(resultSet.getString("type"));
            project.setUrl(resultSet.getString("url"));

            resultSet.close();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method findOne, possible problems with the database");
        }
        return null;
    }

    @Override
    public int create(Project project) throws SQLException {
        int resultCreate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ProjectConstants.STMT_INSERT_ONE)){

            //INSERT INTO users (name, type, url) VALUES (?, ?, ?);

            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getType());
            preparedStatement.setString(3, project.getUrl());

            resultCreate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method create, possible problems with the database");
        }
        return resultCreate;
    }

    @Override
    public List<Project> readAll() throws SQLException {
        List<Project> projectList = new ArrayList<>();

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(ProjectConstants.STMT_READ_ALL);

            while (resultSet.next()){
                Project project = new Project();

                project.setId(resultSet.getLong("id"));
                project.setName(resultSet.getString("name"));
                project.setType(resultSet.getString("type"));
                project.setUrl(resultSet.getString("url"));

                projectList.add(project);
                resultSet.close();
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method readAll, possible problems with the database");

        }
        return projectList;
    }

    @Override
    public int update(Project project) throws SQLException {
        int resultUpdate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ProjectConstants.STMT_UPDATE)){

            //UPDATE projects SET name=?, type=?, url=? WHERE id=?;

            preparedStatement.setString(1, project.getName());
            preparedStatement.setString(2, project.getType());
            preparedStatement.setString(3, project.getUrl());
            preparedStatement.setLong(4, project.getId());

            resultUpdate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method update, possible problems with the database");
        }
        return resultUpdate;
    }

    @Override
    public int delete(Project project) throws SQLException {
        int resultDelete = 0;

        try(java.sql.Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(ProjectConstants.STMT_DELETE)) {
            //DELETE FROM projects WHERE id=?

            preparedStatement.setLong(1, project.getId());
            resultDelete = preparedStatement.executeUpdate();

            if (resultDelete == 0)
                log.log(Level.FINE, "Project with that id does not exists");

        }catch (SQLFeatureNotSupportedException e){
            log.log(Level.SEVERE,"method delete, possible? not found id");

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method delete, possible problems with the database");
        }
        return resultDelete;
    }


    public Long getIdLastProject () throws SQLException{

        Long id = null;

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(ProjectConstants.STMT_LAST_ID);

            if (resultSet.next())
                id = resultSet.getLong(1);
            else
                return null;
        resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method getIdLastProject, possible problems with the database");
        }
        return id;
    }
}
