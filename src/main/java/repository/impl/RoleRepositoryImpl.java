package repository.impl;

import model.Role;
import repository.RoleRepository;
import repository.constanst.RoleConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static repository.config.Connection.getConnection;

public class RoleRepositoryImpl implements RoleRepository{
    private Logger log = Logger.getLogger(RoleRepositoryImpl.class.getName());

    @Override
    public Role findOne(Long id) {
        Role role = null;

        try (java.sql.Connection connection = getConnection();

             PreparedStatement statement = connection.prepareStatement(RoleConstants.STMT_FIND_ONE)){
            //SELECT id, name, description FROM roles WHERE id=?;

            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                log.log(Level.WARNING, "role with id = " + id + " does not exists");
                return null;
            }
            role.setId(resultSet.getLong("id"));

            role.setName(resultSet.getString("name"));
            role.setDescription(resultSet.getString("description"));

            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "method findOne, possible problems with the database");
        }
        return role;
    }

    @Override
    public int create(Role role) throws SQLException {
        int resultCreate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(RoleConstants.STMT_INSERT_ONE)){

            //INSERT INTO roles (name, description) VALUES (?, ?);

            preparedStatement.setString(1, role.getName());
            preparedStatement.setString(2, role.getDescription());

            resultCreate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method create, possible problems with the database");
        }
        return resultCreate;
    }

    @Override
    public List<Role> readAll() throws SQLException {
        List<Role> roleList = new ArrayList<>();

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(RoleConstants.STMT_READ_ALL);

            while (resultSet.next()){
                Role role = new Role();

                role.setId(resultSet.getLong("id"));
                role.setName(resultSet.getString("name"));
                role.setDescription(resultSet.getString("description"));

                roleList.add(role);
            }
            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method readAll, possible problems with the database");

        }
        return roleList;
    }

    @Override
    public int update(Role role) throws SQLException {
        int resultUpdate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(RoleConstants.STMT_UPDATE)){

            //UPDATE roles SET name=?, description=? WHERE id=?;

            preparedStatement.setString(1, role.getName());
            preparedStatement.setString(2, role.getDescription());
            preparedStatement.setLong(3, role.getId());

            resultUpdate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method update, possible problems with the database");
        }
        return resultUpdate;
    }

    @Override
    public int delete(Role role) throws SQLException {
        int resultDelete = 0;

        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(RoleConstants.STMT_DELETE)) {
            //DELETE FROM roles WHERE id=?

            preparedStatement.setLong(1, role.getId());
            resultDelete = preparedStatement.executeUpdate();

            if (resultDelete == 0)
                log.log(Level.FINE, "Role with that id does not exists");

        } catch (SQLFeatureNotSupportedException e){
            log.log(Level.SEVERE,"method delete, possible not found id");

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method delete, possible problems with the database");
        }
        return resultDelete;
    }

    public Long getIdLastRole () throws SQLException {

        Long id = null;

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(RoleConstants.STMT_LAST_ID);

            if (resultSet.next())
                id = resultSet.getLong(1);
            else
                return null;
        resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method getIdLastRole, possible problems with the database");
        }
        return id;
    }
}
