package repository.impl;

import model.Task;
import repository.TaskRepository;
import repository.constanst.TaskConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static repository.config.Connection.getConnection;

public class TaskRepositoryImpl implements TaskRepository {
    private Logger log = Logger.getLogger(TaskRepositoryImpl.class.getName());

    @Override
    public Task findOne(Long id) {
        Task task = new Task();

        try (java.sql.Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(TaskConstants.STMT_FIND_ONE)){

            //SELECT id, name, status, date, project_id FROM tasks WHERE id=?;

            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                log.log(Level.WARNING, "task with id = " + id + " does not exists");
                return null;
            }

            task.setId(resultSet.getLong("id"));

            task.setName(resultSet.getString("name"));
            task.setStatus(resultSet.getString("status"));
            task.setDate(resultSet.getString("date"));
            task.setProjectId(resultSet.getLong("project_id"));

            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "method findOne, possible problems with the database");
        }
        return task;     }

    @Override
    public int create(Task task) throws SQLException {
        int resultCreate = 0;

        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(TaskConstants.STMT_INSERT_ONE)){

            //INSERT INTO tasks (name, status, date, project_id) VALUES (?, ?, ?, ?);

            preparedStatement.setString(1, task.getName());
            preparedStatement.setString(2, task.getStatus());
            preparedStatement.setString(3, task.getDate());
            preparedStatement.setLong(4, task.getProjectId());

            resultCreate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method create, possible problems with the database");
        }
        return resultCreate;
    }

    @Override
    public int update(Task task) throws SQLException {
        int resultUpdate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(TaskConstants.STMT_UPDATE)){

            //UPDATE tasks SET name=?, status=?, date=?, project_id=? WHERE id=?;

            preparedStatement.setString(1, task.getName());
            preparedStatement.setString(2, task.getStatus());
            preparedStatement.setString(3, task.getDate());
            preparedStatement.setLong(4,  task.getProjectId());
            preparedStatement.setLong(5, task.getId());

            resultUpdate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method update, possible problems with the database");
        }
        return resultUpdate;
    }

    @Override
    public int delete(Task task) throws SQLException {
       int resultDelete = 0;

        try(Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(TaskConstants.STMT_DELETE)) {
            //DELETE FROM tasks WHERE id=?

            preparedStatement.setLong(1, task.getId());
            resultDelete = preparedStatement.executeUpdate();

            if (resultDelete == 0)
                log.log(Level.FINE, "Task with that id does not exists");


        }catch (SQLFeatureNotSupportedException e){
            log.log(Level.SEVERE,"method delete, possible not found id");

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method delete, possible problems with the database");
        }
        return resultDelete;
    }


    public List<Task> readAll() throws SQLException {
        List<Task> taskList = new ArrayList<>();

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(TaskConstants.STMT_READ_ALL);
            //SELECT id, name, status, date, project_id FROM tasks;

            while (resultSet.next()){
                Task task = new Task();

                task.setId(resultSet.getLong("id"));
                task.setName(resultSet.getString("name"));
                task.setStatus(resultSet.getString("status"));
                task.setDate(resultSet.getString("date"));
                task.setProjectId(resultSet.getLong("project_id"));

                taskList.add(task);
            }
            resultSet.close();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method readAll, possible problems with the database");

        }
        return taskList;
    }

    public Long getIdLastTask () throws SQLException{

        Long id = null;

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(TaskConstants.STMT_LAST_ID);

            if (resultSet.next())
                id = resultSet.getLong(1);
            else
                return null;

            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method getIdLastTask, possible problems with the database");
        }
        return id;
    }
}
