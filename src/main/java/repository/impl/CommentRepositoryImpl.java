package repository.impl;

import model.Comment;
import repository.CommentRepository;
import repository.constanst.CommentConstants;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import static repository.config.Connection.getConnection;

public class CommentRepositoryImpl implements CommentRepository{
    private Logger log = Logger.getLogger(CommentRepositoryImpl.class.getName());

    @Override
    public Comment findOne(Long id) {
        Comment comment = new Comment();

        try (java.sql.Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(CommentConstants.STMT_FIND_ONE)){

            //SELECT id, content, date, user_id, task_id FROM comments WHERE id=?;

            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                log.log(Level.WARNING, "comment with id = " + id + " does not exists");
                return null;
            }

            comment.setId(resultSet.getLong("id"));

            comment.setContent(resultSet.getString("content"));
            comment.setDate(resultSet.getString("date"));
            comment.setUserId(resultSet.getLong("user_id"));
            comment.setTaskId(resultSet.getLong("task_id"));

            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE, "method findOne, possible problems with the database");
        }
        return comment;
    }

    @Override
    public int create(Comment comment) throws SQLException {
        int resultCreate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CommentConstants.STMT_INSERT_ONE)){

            //INSERT INTO comments (content, date, user_id, task_id) VALUES (?, ?, ?, ?);

            preparedStatement.setString(1, comment.getContent());
            preparedStatement.setString(2, comment.getDate());
            preparedStatement.setLong(3, comment.getUserId());
            preparedStatement.setLong(4, comment.getTaskId());

            resultCreate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE, "method create, possible problems with the database");
        }
        return resultCreate;
    }

    @Override
    public List<Comment> readAll() throws SQLException {
        List<Comment> commentList = new ArrayList<>();

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(CommentConstants.STMT_READ_ALL);
            //SELECT id, content, date, user_id, task_id FROM comments;

            while (resultSet.next()){
                Comment comment = new Comment();

                comment.setId(resultSet.getLong("id"));
                comment.setContent(resultSet.getString("content"));
                comment.setDate(resultSet.getString("date"));
                comment.setUserId(resultSet.getLong("user_id"));
                comment.setTaskId(resultSet.getLong("task_id"));

                commentList.add(comment);
            }
            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method readAll, possible problems with the database");

        }
        return commentList;
    }

    @Override
    public int update(Comment comment) throws SQLException {
        int resultUpdate = 0;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(CommentConstants.STMT_UPDATE)){

            //UPDATE comments SET content=?, date=?, user_id=?, task-id=? WHERE id=?;

            preparedStatement.setString(1, comment.getContent());
            preparedStatement.setString(2, comment.getDate());
            preparedStatement.setLong(3, comment.getUserId());
            preparedStatement.setLong(4, comment.getTaskId());
            preparedStatement.setLong(5, comment.getId());

            resultUpdate = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method update, possible problems with the database");
        }
        return resultUpdate;
    }

    @Override
    public int delete(Comment comment) throws SQLException {
        int resultDelete = 0;

        try (Connection connection = getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(CommentConstants.STMT_DELETE)) {
            //DELETE FROM comments WHERE id=?

            preparedStatement.setLong(1, comment.getId());
            resultDelete = preparedStatement.executeUpdate();

            if (resultDelete == 0)
                log.log(Level.FINE, "Comment with that id does not exists");

        } catch (SQLFeatureNotSupportedException e){
            log.log(Level.SEVERE,"method delete, possible not found id");

        } catch (SQLException e) {
            log.log(Level.SEVERE,"method delete, possible problems with the database");
        }
        return resultDelete;
    }

    public Long getIdLastComment () throws SQLException{

        Long id = null;

        try (java.sql.Connection connection = getConnection();
             Statement statement = connection.createStatement()){

            ResultSet resultSet = statement.executeQuery(CommentConstants.STMT_LAST_ID);

            if (resultSet.next())
                id = resultSet.getLong(1);
            else
                return null;
            resultSet.close();
        } catch (SQLException e) {
            log.log(Level.SEVERE,"method getIdLastComment, possible problems with the database");
        }
        return id;
    }
}
