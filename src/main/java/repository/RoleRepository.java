package repository;

import model.Role;

import java.sql.SQLException;
import java.util.List;

public interface RoleRepository {
    public Role findOne(Long id);

    int create(Role role) throws SQLException;

    List<Role> readAll() throws SQLException;

    int update(Role role) throws SQLException;

    int delete(Role role) throws SQLException;

}
