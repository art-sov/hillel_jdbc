package repository;

import model.Comment;

import java.sql.SQLException;
import java.util.List;

public interface CommentRepository {

    Comment findOne(Long id);

    int create(Comment comment) throws SQLException;

    List<Comment> readAll() throws SQLException;

    int update(Comment comment) throws SQLException;

    int delete(Comment comment) throws SQLException;
}
