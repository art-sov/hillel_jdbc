package repository;

import model.Project;

import java.sql.SQLException;
import java.util.List;

public interface ProjectRepository {
    Project findOne(Long id);

    int create(Project project) throws SQLException;

    List<Project> readAll() throws SQLException;

    int update(Project project) throws SQLException;

    int delete(Project project) throws SQLException;

}
