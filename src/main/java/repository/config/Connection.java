package repository.config;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class Connection {
    private static Properties properties;

    public static void load() {
        properties = new Properties();
        try {
            properties.load(new FileInputStream("/Users/admin/IdeaProjects/HillelSchool/Tasks/enterprise_jdbc/src/main/resources/db-local.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static java.sql.Connection getConnection() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        load();
        String url = "jdbc:mysql://" + properties.getProperty("host") + ":" +  properties.getProperty("port") + "/"
                + properties.getProperty("dbname") + "?useSSL=false";

        try {
            return DriverManager.getConnection(url, properties.getProperty("username"), properties.getProperty("password"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
