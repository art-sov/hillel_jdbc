package repository.constanst;

        import java.util.List;
        import java.util.stream.Collectors;
        import java.util.stream.Stream;

public class ProjectConstants {
    public static final String TABLE = "projects";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String TYPE = "type";
    public static final String URL = "url";

    public static final List<String> COLS_WITHOUT_ID = Stream.of(NAME, TYPE, URL).collect(Collectors.toList());


    //SELECT id, name, type, url FROM projects WHERE id=?;
    public static final String STMT_FIND_ONE = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE + " WHERE " + ID + "=?";


    //INSERT INTO projects (name, type, url) VALUES (?, ?, ?);
    public static final String STMT_INSERT_ONE = "INSERT INTO " + TABLE + " ("
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + ") " + "VALUES (?, ?, ?);";


    //SELECT id, name, type, url FROM projects;
    public  static final String STMT_READ_ALL = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE +";";

    //UPDATE projects SET name=?, type=?, url=? WHERE id=?;
    public static final String STMT_UPDATE = "UPDATE " + TABLE + " SET "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining("=?, ")) + "=?" +" WHERE id=?;";

    public static final String STMT_DELETE = "DELETE FROM " + TABLE + " WHERE id=?";

    public static final String STMT_LAST_ID = "SELECT max(" + ID + ") FROM " + TABLE + ";";
}
