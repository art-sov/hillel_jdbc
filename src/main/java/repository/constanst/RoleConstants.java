package repository.constanst;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RoleConstants {
    public static final String TABLE = "roles";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";


    public static final List<String> COLS_WITHOUT_ID = Stream.of(NAME, DESCRIPTION).collect(Collectors.toList());

    //SELECT id, name, description FROM roles WHERE id=?;
    public static final String STMT_FIND_ONE = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE + " WHERE " + ID + "=?";


    //INSERT INTO roles (name, description) VALUES (?, ?);
    public static final String STMT_INSERT_ONE = "INSERT INTO " + TABLE + " ("
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + ") " + "VALUES (?, ?);";


    //SELECT id, name, description FROM roles;
    public  static final String STMT_READ_ALL = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE +";";

    //UPDATE roles SET name=?, description=? WHERE id=?;
    public static final String STMT_UPDATE = "UPDATE " + TABLE + " SET "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining("=?, ")) + "=?" +" WHERE id=?;";


    public static final String STMT_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + "=?'";

    public static final String STMT_LAST_ID = "SELECT max(" + ID + ") FROM " + TABLE + ";";
}
