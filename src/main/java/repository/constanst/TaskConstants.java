package repository.constanst;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TaskConstants {

    public static final String TABLE = "tasks";

    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String STATUS = "status";
    public static final String DATE = "date";
    public static final String PROJECT_ID = "project_id";

    public static final List<String> COLS_WITHOUT_ID = Stream.of(NAME, STATUS, DATE,
            PROJECT_ID).collect(Collectors.toList());


    //SELECT id, name, status, date, project_id FROM tasks WHERE id=?;
    public static final String STMT_FIND_ONE = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE + " WHERE " + ID + "=?";


    //INSERT INTO tasks (name, status, date, project_id) VALUES (?, ?, ?, ?);
    public static final String STMT_INSERT_ONE = "INSERT INTO " + TABLE + " ("
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + ") " + "VALUES (?, ?, ?, ?);";


    //SELECT id, name, status, date, project_id FROM tasks;
    public  static final String STMT_READ_ALL = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE +";";

    //UPDATE tasks SET name=?, status=?, date=?, project_id=? WHERE id=?;
    public static final String STMT_UPDATE = "UPDATE " + TABLE + " SET "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining("=?, ")) + "=?" +" WHERE id=?;";

    public static final String STMT_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + "=?'";

    public static final String STMT_LAST_ID = "SELECT max(" + ID + ") FROM " + TABLE + ";";

}
