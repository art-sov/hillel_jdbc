package repository.constanst;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CommentConstants {
    public static final String TABLE = "comments";

    public static final String ID = "id";
    public static final String CONTENT = "content";
    public static final String DATE = "date";
    public static final String USER_ID = "user_id";
    public static final String TASK_ID = "task_id";

    public static final List<String> COLS_WITHOUT_ID = Stream.of(CONTENT, DATE, USER_ID,
            TASK_ID).collect(Collectors.toList());

    public static final String STMT_FIND_ONE = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(","))
            + " FROM " + TABLE + " WHERE " + ID + "=?";

    //INSERT INTO comments (content, date, user_id, task_id) VALUES (?, ?, ?, ?);
    public static final String STMT_INSERT_ONE = "INSERT INTO " + TABLE + " ("
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + ") " + "VALUES (?, ?, ?, ?);";

    //SELECT id, content, date, user_id, task_id FROM comments;
    public  static final String STMT_READ_ALL = "SELECT " + ID + ", "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining(", "))
            + " FROM " + TABLE +";";

    //UPDATE comments SET content=?, date=?, user_id=?, task-id=? WHERE id=?;
    public static final String STMT_UPDATE = "UPDATE " + TABLE + " SET "
            + COLS_WITHOUT_ID.stream().collect(Collectors.joining("=?, ")) + "=?" +" WHERE id=?;";

    public static final String STMT_DELETE = "DELETE FROM " + TABLE + " WHERE " + ID + "=?'";

    public static final String STMT_LAST_ID = "SELECT max(" + ID + ") FROM " + TABLE + ";";
}
