package repository;

import model.Task;
import model.User;

import java.sql.SQLException;
import java.util.List;

public interface TaskRepository {
    public Task findOne(Long id);

    int create(Task task) throws SQLException;

    List<Task> readAll() throws SQLException;

    int update(Task task) throws SQLException;

    int delete(Task task) throws SQLException;
}
