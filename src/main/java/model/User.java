package model;

public class User {
    private Long id;
    private String name;
    private String lastname;
    private String password;
    private String email;
    private String lastActive;
    private Long project_id;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getLastActive() {
        return lastActive;
    }

    public Long getProject_id() {
        return project_id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setLastActive(String lastActive) {
        this.lastActive = lastActive;
    }

    public void setProject_id(Long project_id) {
        this.project_id = project_id;
    }
}
