
import model.Project;
import repository.impl.ProjectRepositoryImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

public class TestProject {
    private static Project project;
    private static BufferedReader reader;

    public static void main(String[] args) throws SQLException {
        ProjectRepositoryImpl projectRepository = new ProjectRepositoryImpl();
        List<Project> projectList = projectRepository.readAll();
        project = new Project();
        reader = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Insert a new project: ");

        getProjectInformation();

        Long id = projectRepository.getIdLastProject();
        project.setId(id);

        projectRepository.create(project);
        id = projectRepository.getIdLastProject();
        project.setId(id);

        System.out.println("insert new project successfully, name: " + project.getName() + ", id = " + project.getId());
        projectList.add(project);

        System.out.println();
        System.out.println();
        System.out.println("read all projects from database....");

        for(int i = 0; i < projectList.size(); i++) {
            System.out.println("ID: " + projectList.get(i).getId());
            System.out.println("Name: " + projectList.get(i).getName());
            System.out.println("Type: " + projectList.get(i).getType());
            System.out.println("URL: " + projectList.get(i).getUrl());

            System.out.println("==============================");
        }

        System.out.println("update the project: ");
        getProjectInformation();
        try{
            System.out.println("insert id: ");
            project.setId(Long.parseLong(reader.readLine()));
        }catch (IOException e) {
            e.printStackTrace();
        }
        projectRepository.update(project);

        System.out.println("update successfully");
        System.out.println("read project from database with id = " + project.getId() + "...");

        projectRepository.findOne(project.getId());

        System.out.println("id: " + project.getId());
        System.out.println("name: " + project.getName());
        System.out.println("type: " + project.getType());
        System.out.println("url: " + project.getUrl());

        System.out.println();

        try{
            System.out.println("delete user, insert id: ");
            project.setId(Long.parseLong(reader.readLine()));
        }catch (IOException e) {
            e.printStackTrace();
        }
        projectRepository.delete(project);
        System.out.println("delete successfully");
    }

    public static void getProjectInformation(){

        try {
            System.out.println("Insert name: ");
            project.setName(reader.readLine());

            System.out.println("Insert type: ");
            project.setType(reader.readLine());

            System.out.println("Insert url: ");
            project.setUrl(reader.readLine());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
